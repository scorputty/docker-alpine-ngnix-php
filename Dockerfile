FROM alpine:3.12.4

ENV S6VERSION=v2.2.0.3

USER 0

# Install packages
RUN set -ex \
 && apk --no-cache add \
      nginx \
      curl \
      git \
      php-fpm \
 && curl -sSfL "https://github.com/just-containers/s6-overlay/releases/download/${S6VERSION}/s6-overlay-amd64.tar.gz" | tar -xzv -C / \
 && rm -Rf /var/www/*

# Copy configuration files to root
COPY rootfs /

# Fix permissions
RUN chown -Rf nginx:www-data /var/www/


WORKDIR /var/www
EXPOSE 80 443 8080

ENTRYPOINT ["/init"]
