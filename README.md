# docker-alpine-ngnix-php

Very small Docker for demo purposes

* Original from:
- https://github.com/luzifer-docker/alpine-nginx-php

## howto deploy on openshift 
### create namespace
```oc create namespace stef-ngnix```
### allow anyuid (for testing)
```oc adm policy add-scc-to-user anyuid system:serviceaccount:stef-ngnix:default```
### create new app
```oc new-app https://gitlab.com/scorputty/docker-alpine-ngnix-php.git --name=ngnix-test-container -n stef-ngnix```
### expose route
```oc expose service/ngnix-test-container```